using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonScript : MonoBehaviour
{
    public Image buttonStart;
    public Sprite activeButton; 
    public void ActionButton(int id){
        StartCoroutine(DelayAnimationAction(.2f,id));
    }
    IEnumerator DelayAnimationAction(float second, int index){
        buttonStart.sprite = activeButton;
        yield return new WaitForSeconds(second);
        GameController.instance.LoadLevel(index);
    }
}
